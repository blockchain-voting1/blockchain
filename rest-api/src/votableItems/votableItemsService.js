const fabric = require('./../common/services/fabric');

exports.findByElectionId = async (electionId) => {
    let candidates = await fabric.queryByObjectType('votableItem');
    return candidates.filter((votableItem) => {
        return votableItem.Record.electionId === electionId;
    });
}

exports.findResultsByElectionId = async (electionId) => {
    let candidates = await this.findByElectionId(electionId);

    candidates = candidates.map((candidate) => {
        return {
            votableId: candidate.Record.votableId,
            img: candidate.Record.img,
            description: candidate.Record.description,
            politicalParty: candidate.Record.politicalParty.name,
            votes: candidate.Record.count
        }
    });

    return candidates;
}
