const config = require('./config');
const network = require('./network');
const walletFactory = require('./walletFactory');

const appAdmin = config.appAdmin;

exports.queryByObjectType = async (objectType) => {
  const wallet = walletFactory.createFromFileSystem();
  const networkObj = await network.connectToNetwork(
    appAdmin,
    wallet
  );
  const buffer = await network.invoke(
    networkObj,
    true,
    "queryByObjectType",
    objectType
  );

  return JSON.parse(JSON.parse(buffer));
}

exports.registerVoter = async body => {
  const fileWallet = walletFactory.createFromFileSystem();

  let identity = await network.registerIdentity(
    body.voterId,
    body.registrarId,
    body.name,
    body.email
  );

  if (identity.error) {
    throw new Error(identity.error);
  }

  let networkObj = await network.connectToNetwork(
    body.voterId,
    fileWallet
  );

  if (networkObj.error) {
    throw new Error(networkObj.error);
  }

  const buffer = await network.invoke(
    networkObj,
    false,
    'createVoter',
    [JSON.stringify(body)]
  );

  if (buffer.error) {
    throw new Error(buffer.error);
  }

  await fileWallet.delete(body.voterId);

  return identity;
}

exports.castVote = async body => {
  const fileWallet = walletFactory.createFromFileSystem();
  await fileWallet.import(body.voterId, body.identity)

  let networkObj = await network.connectToNetwork(
      body.voterId,
      fileWallet
  );

  if (networkObj.error) {
    throw new Error(networkObj.error);
  }
  
  const vote = JSON.parse(await network.invoke(
      networkObj,
      false,
      'castVote',
      [JSON.stringify(body)]
  ));

  if (vote.error) {
    throw new Error(vote.error);
  }

  await fileWallet.delete(body.voterId);

  return vote;
}
