const path = require("path");
const { FileSystemWallet, InMemoryWallet } = require("fabric-network");

exports.createFromFileSystem = () => {
  return new FileSystemWallet(path.join(process.cwd(), "wallet"));
}

exports.createFromMemory = () => {
  return new InMemoryWallet();
}
