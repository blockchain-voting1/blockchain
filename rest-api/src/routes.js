const express = require("express");
const router = express.Router();
const electionsRoutes = require('./elections/electionsRoutes');
const votersRoutes = require('./voters/votersRoutes');

router.use("/elections", electionsRoutes);
router.use("/voters", votersRoutes);

module.exports = router;
