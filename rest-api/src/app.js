'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
// const path = require('path');
// const fs = require('fs');
const appRoutes = require("./routes");

const app = express();

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

// const configPath = path.join(process.cwd(), './config.json');
// const configJSON = fs.readFileSync(configPath, 'utf8');
// const config = JSON.parse(configJSON);


app.use("/", appRoutes);


app.listen(process.env.PORT || 8081);