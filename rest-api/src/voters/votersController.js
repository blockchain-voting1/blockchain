const service = require('./votersService');

exports.registerVoter = async ({body}, res) => {
  try {
    const identity = await service.create(body);
    return res.json({
      message: `Successfully registered voter ${body.name}. Use voterId ${body.voterId} to login above.`,
      identity: identity
    });
  } catch (err) {
    return res
      .status(500)
      .json({ message: err.message });
  }
}
