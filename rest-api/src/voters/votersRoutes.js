const express = require("express");
const router = express.Router();

const controller = require('./votersController');

router.post("/", controller.registerVoter);

module.exports = router;
