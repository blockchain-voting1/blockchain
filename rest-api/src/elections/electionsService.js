const fabric = require('./../common/services/fabric');

exports.getAll = () => {
    return fabric.queryByObjectType('election');
}

exports.findById = async (electionId) => {
    let elections = await fabric.queryByObjectType('election');
    return elections.find((election) => {
        return election.Record.electionId === electionId;
    }).Record;
}

exports.vote = async (body, electionId) => {
    return fabric.castVote({
        identity: body.identity,
        voterId: body.voterId,
        picked: body.votableId,
        electionId: electionId
    });
}