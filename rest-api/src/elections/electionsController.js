const service = require('./electionsService');
const votableItemsService = require('../votableItems/votableItemsService');


exports.list = async (_, res) => {
    return res.json(await service.getAll());
};

exports.findVotableItems = async (req, res) => {
    return res.json(await votableItemsService.findByElectionId(req.params.electionId));
}

exports.results = async (req, res) => {
    const electionId = req.params.electionId;
    let election = await service.findById(electionId);
    election.candidates = await votableItemsService.findResultsByElectionId(electionId);
    election.totalVotes = election.candidates.reduce((sum, candidate) => {
        return sum + candidate.votes;
    }, 0);

    return res.json(election);
}

exports.vote = async (req, res) => {
    try {
        await service.vote(req.body, req.params.electionId);
        return res.json({
            message: `Successfully voted for ${req.body.votableId} in ${req.params.electionId}.`
        });
    } catch (err) {
        return res
            .status(500)
            .json({message: err.message});
    }
};