const express = require("express");
const router = express.Router();

const controller = require('./electionsController');

router.get("/", controller.list);
router.post("/:electionId/vote", controller.vote)
router.get("/:electionId/votable-items", controller.findVotableItems)
router.get("/:electionId/results", controller.results)

module.exports = router;
