/* eslint-disable indent */
'use strict';

class VotableItem {

    /**
   *
   * VotableItem
   *
   * Constructor for a VotableItem object. These will eventually be placed on the
   * ballot.
   *
   * @param votableId - the Id of the votableItem
   * @param description - the description of the votableItem
   * @param position - the position of the votableItem
   * @param img - the image url of the votableItem
   * @param politicalParty - the political party of the votable item
   * @param vice - the votable item vice
   * @param voterId - the unique Id which corresponds to a registered voter
   * @returns - registrar object
   */
  constructor(
    ctx,
    electionId,
    votableId,
    description,
    position,
    img,
    politicalParty,
    vice
  ) {
    
    this.electionId = electionId;
    this.votableId = votableId;
    this.description = description;
    this.count = 0;
    this.type = 'votableItem';
    this.position = position;
    this.img = img;
    this.politicalParty = politicalParty;
    this.vice = vice;

    if (this.__isContract) {
      delete this.__isContract;
    }
    return this;

  }
}
module.exports = VotableItem;